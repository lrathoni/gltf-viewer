#include "ViewerApplication.hpp"

#include <cassert>
#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

#include "./utils/gltf.hpp"
#include "./utils/images.hpp"

#include <imgui.h>

#include <cmath>

// shaders/forward.vs.glsl, using the corresponding layout(location = ...))
GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

bool ViewerApplication::loadGltfFile(tinygltf::Model &model)
{
  assert(&model != NULL);
  tinygltf::TinyGLTF loader;
  std::string err;
  std::string warn;

  bool ret =
      loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }

  if (!ret) {
    printf("Failed to parse glTF file\n");
  }

  return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(
    const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects(
      model.buffers.size(), 0); // Assuming buffers is a std::vector of Buffer

  // Correction why cast to GLsizei ?? --> ask to Laurent
  glGenBuffers(model.buffers.size(), bufferObjects.data());
  for (size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER,
        model.buffers[i].data.size(), // Assume a Buffer has a data member
                                      // variable of type std::vector
        model.buffers[i].data.data(), 0);
  }
  glBindBuffer(
      GL_ARRAY_BUFFER, 0); // Cleanup the binding point after the loop only

  return bufferObjects;
}

std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model,
    // variable previously created
    const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshIndexToVaoRange)
{
  std::vector<GLuint> vertexArrayObjects;

  for (auto meshIdx = 0; meshIdx < model.meshes.size(); ++meshIdx) {
    const auto vaoOffset = vertexArrayObjects.size();
    const auto meshSize = model.meshes[meshIdx].primitives.size();

    vertexArrayObjects.resize(vaoOffset + meshSize);

    glGenVertexArrays(meshSize, &vertexArrayObjects[vaoOffset]);

    meshIndexToVaoRange.push_back(VaoRange{GLsizei(vaoOffset),
        GLsizei(meshSize)}); // Will be used during rendering

    auto primitive = model.meshes[meshIdx].primitives;

    for (auto primitiveIdx = 0; primitiveIdx < meshSize; ++primitiveIdx) {
      glBindVertexArray(vertexArrayObjects[vaoOffset + primitiveIdx]);

      // POSITION -------------------------------------
      {
        const auto iterator =
            primitive[primitiveIdx].attributes.find("POSITION");

        if (iterator !=
            end(primitive[primitiveIdx]
                    .attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the
          // value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          // https://stackoverflow.com/questions/50074638/c-get-vertex-arrays-from-buffer-gltf-model-loader
          const auto &accessor =
              model.accessors[accessorIdx]; // TODO get the correct
                                            // tinygltf::Accessor from
                                            // model.accessors
          const auto &bufferView =
              model.bufferViews[accessor.bufferView]; // TODO get the correct
                                                      // tinygltf::BufferView
                                                      // from model.bufferViews.
                                                      // You need to use the
                                                      // accessor
          const auto bufferIdx =
              bufferView.buffer; // TODO get the index of the buffer used by the
                                 // bufferView (you need to use it)

          const auto bufferObject =
              bufferObjects[bufferIdx]; // TODO get the correct buffer object
                                        // from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray (you need to use
          // VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of
          // the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_POSITION_IDX);

          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset =
              accessor.byteOffset +
              bufferView.byteOffset; // TODO Compute the total byte offset using
                                     // the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type,
              accessor.componentType, GLsizei(bufferView.byteStride), GL_FALSE,
              (GLvoid *)byteOffset);
        }
      }
      //--------------------------------------------------------------------------

      // NORMAL -------------------------------------
      {
        const auto iterator = primitive[primitiveIdx].attributes.find("NORMAL");

        if (iterator !=
            end(primitive[primitiveIdx]
                    .attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the
          // value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          // https://stackoverflow.com/questions/50074638/c-get-vertex-arrays-from-buffer-gltf-model-loader
          const auto &accessor =
              model.accessors[accessorIdx]; // TODO get the correct
                                            // tinygltf::Accessor from
                                            // model.accessors
          const auto &bufferView =
              model.bufferViews[accessor.bufferView]; // TODO get the correct
                                                      // tinygltf::BufferView
                                                      // from model.bufferViews.
                                                      // You need to use the
                                                      // accessor
          const auto bufferIdx =
              bufferView.buffer; // TODO get the index of the buffer used by the
                                 // bufferView (you need to use it)

          const auto bufferObject =
              bufferObjects[bufferIdx]; // TODO get the correct buffer object
                                        // from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray (you need to use
          // VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of
          // the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_NORMAL_IDX);

          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset =
              accessor.byteOffset +
              bufferView.byteOffset; // TODO Compute the total byte offset using
                                     // the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type,
              accessor.componentType, GLsizei(bufferView.byteStride), GL_FALSE,
              (GLvoid *)byteOffset);
        }
      }
      //--------------------------------------------------------------------------

      // TEXCOORD_0 -------------------------------------
      {
        const auto iterator =
            primitive[primitiveIdx].attributes.find("TEXCOORD_0");

        if (iterator !=
            end(primitive[primitiveIdx]
                    .attributes)) { // If "POSITION" has been found in the map
          // (*iterator).first is the key "POSITION", (*iterator).second is the
          // value, ie. the index of the accessor for this attribute
          const auto accessorIdx = (*iterator).second;
          // https://stackoverflow.com/questions/50074638/c-get-vertex-arrays-from-buffer-gltf-model-loader
          const auto &accessor =
              model.accessors[accessorIdx]; // TODO get the correct
                                            // tinygltf::Accessor from
                                            // model.accessors
          const auto &bufferView =
              model.bufferViews[accessor.bufferView]; // TODO get the correct
                                                      // tinygltf::BufferView
                                                      // from model.bufferViews.
                                                      // You need to use the
                                                      // accessor
          const auto bufferIdx =
              bufferView.buffer; // TODO get the index of the buffer used by the
                                 // bufferView (you need to use it)

          const auto bufferObject =
              bufferObjects[bufferIdx]; // TODO get the correct buffer object
                                        // from the buffer index

          // TODO Enable the vertex attrib array corresponding to POSITION with
          // glEnableVertexAttribArray (you need to use
          // VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of
          // the cpp file)
          glEnableVertexAttribArray(VERTEX_ATTRIB_TEXCOORD0_IDX);

          // TODO Bind the buffer object to GL_ARRAY_BUFFER
          glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

          const auto byteOffset =
              accessor.byteOffset +
              bufferView.byteOffset; // TODO Compute the total byte offset using
                                     // the accessor and the buffer view
          // TODO Call glVertexAttribPointer with the correct arguments.
          glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type,
              accessor.componentType, GLsizei(bufferView.byteStride), GL_FALSE,
              (GLvoid *)byteOffset);
        }
      }
      //--------------------------------------------------------------------------
      // Remember size is obtained with accessor.type, type is obtained with
      // accessor.componentType. The stride is obtained in the bufferView,
      // normalized is always GL_FALSE, and pointer is the byteOffset (don't
      // forget the cast).
      const auto indices = primitive[primitiveIdx].indices;
      if (indices >= 0) {
        const auto &primAccessor = model.accessors[indices];
        const auto &primBufferView = model.bufferViews[primAccessor.bufferView];
        const auto primBufferIdx = primBufferView.buffer;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[primBufferIdx]);
      }
    }
  }
  glBindVertexArray(0);
  return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(
    const tinygltf::Model &model) const
{
  size_t nbTextures = model.textures.size();
  // We already know the size of vector, no need to use push for each loop
  std::vector<GLuint> textureObjects(nbTextures, 0);

  // Create a default sampler if it not already exist
  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  // select active texture unit
  glActiveTexture(GL_TEXTURE0);

  // Generate nbTextures textures
  glGenTextures(GLsizei(nbTextures), textureObjects.data());

  // Assume a texture object has been created and bound to GL_TEXTURE_2D
  for (size_t i = 0; i < nbTextures; ++i) {

    // Here we assume a texture object has been created and bound to
    // GL_TEXTURE_2D
    const auto &texture = model.textures[i]; // get i-th texture
    assert(texture.source >= 0);             // ensure a source image is present
    const auto &image = model.images[texture.source]; // get the image

    // fill the texture object with the data from the image
    // Bind texture object to target GL_TEXTURE_2D:
    glBindTexture(GL_TEXTURE_2D, textureObjects[i]);
    // Set image data:
    // void glTexImage2D(GLenum target, GLint level, GLint internalformat,
    //    GLsizei width, GLsizei height, GLint border, GLenum format, GLenum
    //    type, const void *data);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
        GL_RGBA, image.pixel_type, image.image.data());

    // Set sampling parameters:
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Set wrapping parameters:
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

    // Create a sampler
    const auto &sampler =
        // return sampler index associatied
        texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    // Specific for textures
    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }
  }
  // Unbind texture
  glBindTexture(GL_TEXTURE_2D, 0);

  return textureObjects;
};

int ViewerApplication::run()
{
  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");
  const auto uDirLight = glGetUniformLocation(glslProgram.glId(), "uDirection");
  const auto uIntLight = glGetUniformLocation(glslProgram.glId(), "uIntensity");
  const auto uBaseColorTexture =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
  const auto uBaseColorFactor =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");
  const auto uMetallicFactor =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
  const auto uRoughnessFactor =
      glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");
  const auto uMetallicRoughnessTexture =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
  const auto uEmissiveTexture =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
  const auto uEmissiveFactor =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");
  const auto uOcclusionTexture =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
  const auto uOcclusionTexStrength =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionTexStrength");
  const auto uApplyOcclusion =
      glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");

  // Get value to send to shader
  glm::vec3 lightDirection = glm::vec3(1, 1, 1);
  glm::vec3 lightIntensity = glm::vec3(1, 1, 1);
  bool lightFromCam = false;
  bool applyOcclusion = true;

  tinygltf::Model model;
  // TODO Loading the glTF file
  if (!loadGltfFile((model))) {
    return -1;
  }

  glm::vec3 bboxMin;
  glm::vec3 bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);

  glm::vec3 bbDiag = bboxMin - bboxMax;
  glm::vec3 bbCenter = (bboxMax + bboxMin) * 0.5f;
  glm::vec3 up = glm::vec3(0, 1, 0);

  // Build projection matrix
  auto maxDistance = float(glm::length(bbDiag));
  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
          0.001f * maxDistance, 1.5f * maxDistance);

  // TODO Implement a new CameraController model and use it instead. Propose the
  // choice from the GUI
  std::unique_ptr<CameraController> cameraController =
      std::make_unique<FirstPersonCameraController>(
          m_GLFWHandle.window(), 0.5f * maxDistance);

  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    // TODO Use scene bounds to compute a better default camera
    cameraController->setCamera(Camera{bbCenter - bbDiag, bbCenter, up});

    glm::vec3 bbEye = bbDiag.z > 0 ? bbCenter + bbDiag
                                   : bbCenter + 2.f * glm::cross(bbDiag, up);
    cameraController->setCamera(Camera{bbEye, bbCenter, up});
  }

  // Set white pixel to texture -----------------------------------------
  std::vector<GLuint> textureObjects = createTextureObjects(model);
  float white[] = {1, 1, 1, 1};
  GLuint whiteTexture;
  // Generate the texture object:
  glGenTextures(1, &whiteTexture);
  // Bind texture object to target GL_TEXTURE_2D:
  glBindTexture(GL_TEXTURE_2D, whiteTexture);
  // Set image data:
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGB, GL_FLOAT, white);
  // Set sampling parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // Set wrapping parameters:
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

  glBindTexture(GL_TEXTURE_2D, 0);

  // TODO Creation of Buffer Objects
  std::vector<GLuint> bufferObjects =
      ViewerApplication::createBufferObjects(model);

  // TODO Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  std::vector<GLuint> VAOs =
      createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  // Lambda function to bind material
  const auto bindMaterial = [&](const auto materialIndex) {
    if (materialIndex >= 0) {
      const auto &material = model.materials[materialIndex];
      const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;

      // base color texture
      // ----------------------------------------------------------------------
      if (uBaseColorFactor >= 0) {
        glUniform4f(uBaseColorFactor,
            (float)pbrMetallicRoughness.baseColorFactor[0],
            (float)pbrMetallicRoughness.baseColorFactor[1],
            (float)pbrMetallicRoughness.baseColorFactor[2],
            (float)pbrMetallicRoughness.baseColorFactor[3]);
      }

      if (uBaseColorTexture >= 0) {
        // only valid if pbrMetallicRoughness.baseColorTexture.index >=
        // 0:
        auto textObject = whiteTexture;
        if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
          // Get the texture concerned in model
          const auto &texture =
              model.textures[pbrMetallicRoughness.baseColorTexture.index];
          // Check if the texture exists (has an associated idx) in the
          // generated array
          if (texture.source >= 0) {
            // association the the textures generate before in the run
            textObject = textureObjects[texture.source];
          }
          glActiveTexture(GL_TEXTURE0);
          // if mat idx >= 0, apply the corresponding one
          glBindTexture(GL_TEXTURE_2D, textObject);
          glUniform1i(uBaseColorTexture, 0);
        }

        // set metallic uniforms (as uBaseColor)
        // ---------------------------------------------------------------------------
        if (uMetallicFactor >= 0) {
          glUniform1f(
              uMetallicFactor, (float)pbrMetallicRoughness.metallicFactor);
        }
        if (uRoughnessFactor >= 0) {
          glUniform1f(
              uRoughnessFactor, (float)pbrMetallicRoughness.roughnessFactor);
        }
        if (uMetallicRoughnessTexture >= 0) {
          auto textObject = whiteTexture;
          if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
            const auto &texture =
                model.textures[pbrMetallicRoughness.metallicRoughnessTexture
                                   .index];
            if (texture.source >= 0) {
              textObject = textureObjects[texture.source];
            }
          }
          glActiveTexture(GL_TEXTURE1);
          glBindTexture(GL_TEXTURE_2D, textObject);
          glUniform1i(uMetallicRoughnessTexture, 1);
        }

        // Emissive texture
        // -----------------------------------------------------------------------------
        if (uEmissiveFactor >= 0) {
          glUniform3f(uEmissiveFactor, (float)material.emissiveFactor[0],
              (float)material.emissiveFactor[1],
              (float)material.emissiveFactor[2]);
        }
        if (uEmissiveTexture >= 0) {
          auto textObject = whiteTexture;
          if (material.emissiveTexture.index >= 0) {
            const auto &texture =
                model.textures[material.emissiveTexture.index];
            if (texture.source >= 0) {
              textObject = textureObjects[texture.source];
            }
          }
          glActiveTexture(GL_TEXTURE2);
          glBindTexture(GL_TEXTURE_2D, textObject);
          glUniform1i(uMetallicRoughnessTexture, 2);
        }

        // Occlusion texture
        // ---------------------------------------------------------------------------------
        if (uOcclusionTexStrength >= 0) {
          glUniform1f(
              uOcclusionTexStrength, (float)material.occlusionTexture.strength);
        }
        if (uOcclusionTexture >= 0) {
          auto textObject = whiteTexture;
          if (material.occlusionTexture.index >= 0) {
            const auto &texture =
                model.textures[material.occlusionTexture.index];
            if (texture.source >= 0) {
              textObject = textureObjects[texture.source];
            }
          }
          glActiveTexture(GL_TEXTURE3);
          glBindTexture(GL_TEXTURE_2D, textObject);
          glUniform1i(uOcclusionTexture, 3);
        }
      }
    }
    // if there is no material
    else {
      // base color texture
      // -----------------------------------------------------------------
      if (uBaseColorFactor >= 0) {
        glUniform4f(uBaseColorFactor, 1, 1, 1, 1);
      }
      if (uBaseColorTexture >= 0) {
        glActiveTexture(GL_TEXTURE0);
        // default material, if there is no material
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(uBaseColorTexture, 0);
      }
      // Metallic Roughness texture
      // -----------------------------------------------------------------------------------
      if (uMetallicFactor >= 0) {
        glUniform1f(uMetallicFactor, 1.f);
      }
      if (uRoughnessFactor >= 0) {
        glUniform1f(uRoughnessFactor, 1.f);
      }
      if (uMetallicRoughnessTexture >= 0) {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTexture, 1);
      }

      // Emissive texture
      // ---------------------------------------------------------------------------------------
      if (uEmissiveFactor >= 0) {
        glUniform3f(uEmissiveFactor, 0.f, 0.f, 0.f);
      }
      if (uEmissiveTexture >= 0) {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uMetallicRoughnessTexture, 2);
      }

      // Occlusion texture
      // ---------------------------------------------------------------------------------
      if (uOcclusionTexStrength >= 0) {
        glUniform1f(uOcclusionTexStrength, 0.f);
      }
      if (uOcclusionTexture >= 0) {
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform1i(uOcclusionTexture, 3);
      }
    }
  };

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const auto viewMatrix = camera.getViewMatrix();

    // Send Light vectors to shader
    if (uDirLight >= 0) {
      if (!lightFromCam) {
        const auto normalizedlightDirection = glm::normalize(
            glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));

        glUniform3f(uDirLight, normalizedlightDirection[0],
            normalizedlightDirection[1], normalizedlightDirection[2]);
      } else
        glUniform3f(uDirLight, 0., 0., 1);
    }
    if (uIntLight >= 0) {
      glUniform3f(
          uIntLight, lightIntensity[0], lightIntensity[1], lightIntensity[2]);
    }

    // Demander à Laurent pourquoi
    if (uApplyOcclusion >= 0) {
      glUniform1i(uApplyOcclusion, applyOcclusion);
    }

    // The recursive function that should draw a node
    // We use a std::function because a simple lambda cannot be recursive
    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          // TODO The drawNode function
          tinygltf::Node c_node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(c_node, parentMatrix);
          if (c_node.mesh >= 0) {

            glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;

            glm::mat4 modelViewProjectionMatrix = projMatrix * modelViewMatrix;

            glm::mat4 normalMatrix = glm::transpose(inverse(modelViewMatrix));

            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(modelViewMatrix));

            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(modelViewProjectionMatrix));

            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE,
                glm::value_ptr(normalMatrix));

            // My code
            // int meshIdx = c_node.mesh;
            // auto mesh = model.meshes[meshIdx].primitives;

            // Correction
            const auto &mesh = model.meshes[c_node.mesh];
            const auto &vaoRange = meshIndexToVaoRange[c_node.mesh];

            for (size_t primIdx = 0; primIdx < mesh.primitives.size();
                 ++primIdx) {

              auto &primitive = mesh.primitives[primIdx];

              // bind material
              // only valid is materialIndex >= 0
              const auto matIdx = primitive.material;
              bindMaterial(matIdx);
              // My code
              // GLuint vao = VAOs[meshIndexToVaoRange[primIdx].begin];
              // Correcttion
              GLuint vao = VAOs[vaoRange.begin + primIdx];
              glBindVertexArray(vao);

              if (primitive.indices >= 0) {
                auto accessor = model.accessors[primitive.indices];
                auto bufferView = model.bufferViews[accessor.bufferView];
                const auto byteOffset =
                    accessor.byteOffset + bufferView.byteOffset;
                glDrawElements(primitive.mode, GLsizei(accessor.count),
                    accessor.componentType, (const GLvoid *)byteOffset);
              } else {
                const auto accessorIdx = (*begin(primitive.attributes)).second;
                const auto &accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }
          for (auto child : c_node.children)
            drawNode(child, modelMatrix);
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      // TODO Draw all nodes
      for (auto node : model.scenes[model.defaultScene].nodes) {
        drawNode(node, glm::mat4(1));
      }
    }
  };

  if (!m_OutputPath.empty()) {
    // array of rgb pixels (3 char), W*H pixels
    std::vector<unsigned char> pixels(3, m_nWindowWidth * m_nWindowHeight);
    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(),
        [&]() { drawScene(cameraController->getCamera()); });
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());
    const auto strPath = m_OutputPath.string();
    stbi_write_png(
        strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
    return 0;
  }

  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();
    // Test
    // ImGui::ShowDemoWindow();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        // Create cameras switcher
        static int camera_choice = 0;

        const bool hasSwitched =
            ImGui::RadioButton("FirstPersonCamera", &camera_choice, 0) ||
            ImGui::RadioButton("TrackballCamera", &camera_choice, 1);

        if (hasSwitched) {
          const auto lastCamParam = cameraController->getCamera();
          if (camera_choice) {
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          } else {
            cameraController = std::make_unique<FirstPersonCameraController>(
                m_GLFWHandle.window(), 0.5f * maxDistance);
          }
          cameraController->setCamera(lastCamParam);
        }

        // Demander à Laurent pourquoi il faut ça
        // correction add ImGuiTreeNodeFlags_DefaultOpen
        if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
          static float theta = 0.f;
          static float phi = 0.f;

          if (ImGui::SliderFloat("theta", &theta, 0, glm::pi<float>()) ||
              ImGui::SliderFloat("phi", &phi, 0, 2.f * glm::pi<float>())) {
            const float sinTheta = glm::sin(theta);
            const float sinPhi = glm::sin(phi);
            const float cosTheta = glm::cos(theta);
            const float cosPhi = glm::cos(phi);
            lightDirection =
                glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
          }
        }

        static float colorLight[3] = {1.f, 1.f, 1.f};
        static float intensityFactor = 1.f;
        if (ImGui::ColorEdit3("lightColor&Intensity", colorLight) ||
            ImGui::InputFloat("intensityFactor", &intensityFactor)) {
          lightIntensity =
              glm::vec3(colorLight[0], colorLight[1], colorLight[2]) *
              intensityFactor;
        }
        ImGui::Checkbox("light from camera", &lightFromCam);
        ImGui::Checkbox("apply occlusion", &applyOcclusion);
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  std::cout << "lookatArgs size " << lookatArgs.size() << std::endl;
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}
