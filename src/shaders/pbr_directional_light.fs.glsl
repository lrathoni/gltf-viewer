#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uDirection;
uniform vec3 uIntensity;

uniform sampler2D uBaseColorTexture;
uniform vec4 uBaseColorFactor;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform sampler2D uMetallicRoughnessTexture;

uniform sampler2D uEmissiveTexture;
uniform vec3 uEmissiveFactor;

uniform sampler2D uOcclusionTexture;
uniform float uOcclusionTexStrength;

uniform int uApplyOcclusion;


out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
  return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

void main()
{

    vec3 N = normalize(vViewSpaceNormal);
    vec3 L = uDirection;
    vec3 V = normalize(-vViewSpacePosition); // normalized vector from the shading location to the eye
    vec3 H = normalize(L + V); //half vector

    vec4 baseColorFromTexture =
      SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
    vec4 baseColor = baseColorFromTexture * uBaseColorFactor; 


    vec4 metallicRougnessFromTexture =
      texture(uMetallicRoughnessTexture, vTexCoords);
    vec3 metallic = vec3(uMetallicFactor * metallicRougnessFromTexture.b);
    float roughness = uRoughnessFactor * metallicRougnessFromTexture.g;

    float NdotL = clamp(dot(N, L), 0., 1.);
    float LdotN = clamp(dot(L, N), 0., 1.);

    float NdotV = clamp(dot(N, V), 0., 1.);
    float VdotN = clamp(dot(V, N), 0., 1.);

    float HdotV = clamp(dot(H, V), 0., 1.);
    float VdotH = clamp(dot(V, H), 0., 1.);

    float HdotL = clamp(dot(H, L), 0., 1.);
    float NdotH = clamp(dot(N, H), 0., 1.);
    
    vec3 diffuse = baseColor.rgb * M_1_PI;

    // You need to compute baseShlickFactor first
    //J'aurai jamais trouvé que c'était ça // le model donne la formule de schlick pas le coeff
    float baseShlickFactor = 1 - abs(VdotH);
    float shlickFactor = baseShlickFactor * baseShlickFactor; // power 2
    shlickFactor *= shlickFactor; // power 4
    shlickFactor *= baseShlickFactor; // power 5

    const float dielectricSpecular = 0.04;
    const float black = 0;

    //mix(A, B, x) --> linearly interpolate between two values // here vec3 type
    vec3 c_diff = mix(baseColor.rgb * (1 - dielectricSpecular), vec3(black), metallic);
    vec3 f0 = mix( vec3(dielectricSpecular), baseColor.rgb, metallic);
    float alpha = roughness * roughness;

    vec3 F = f0 + (vec3(1) - f0) * shlickFactor;

    vec3 f_diffuse = (1 - F) * (1 / M_1_PI) * c_diff;
    
    
    float alpha_2 = alpha * alpha;
    //Calcul of D
    float D_ = NdotH * NdotH * ((alpha_2 - 1.) + 1.);
    float D =  alpha_2 * NdotH  / (M_1_PI) * (D_*D_); 

    //Calcul of V
    float num = HdotL * HdotV; 
    float denum = (abs(NdotL) + sqrt( alpha_2 + (1-alpha_2)*(NdotL*NdotL))) * ( abs(NdotV) + sqrt( alpha_2 + (1-alpha_2)*(NdotL*NdotL)) ); 
    float Vis = denum > 0. ? num/denum : 0.0f;

    vec3 f_specular = F * D * Vis;


    vec3 emissive = SRGBtoLINEAR(texture2D(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;
    vec3 color = (f_diffuse + f_specular) * uIntensity * NdotL;

    //fColor = LINEARtoSRGB(diffuse * uIntensity * NdotL);
    color += emissive;

    if (1 == uApplyOcclusion) {
    float occlusion_text = texture2D(uOcclusionTexture, vTexCoords).r;
    color = mix(color, color * occlusion_text, uOcclusionTexStrength);
  }

    fColor = LINEARtoSRGB(color);
}