#version 330

//Check code in magenta.fs.glsl

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uDirection;
uniform vec3 uIntensity;

out vec3 fColor;

void main()
{
   // Need another normalization because interpolation of vertex attributes does not maintain unit length
    float piInverse = 1./3.14;
    vec3 BRDF = vec3(piInverse);
    vec3 viewSpaceNormal = normalize(vViewSpaceNormal);
    //Formula in text
    fColor = BRDF * uIntensity * dot(viewSpaceNormal, uDirection);
}